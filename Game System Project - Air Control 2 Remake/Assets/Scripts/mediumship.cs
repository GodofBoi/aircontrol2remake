﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mediumship : MonoBehaviour
{
    // A list of vector 3s to keep track of points during line draw
    public List<Vector3> directionalPoints = new List<Vector3>();

    public bool drawingLine;

    // A public group of lists to contain crate types
    
    List<GameObject> crates;

    // A public list that keeps track of the amount of docks and also sends the ships to the right dock
    public List<GameObject> Docks;

    // A tranform varaible for the docking positions
    public Transform greenOneEnd;
    public Transform blueOneEnd;

    //Controls the speeds of the ships
    public float shipSpeeds;

    public bool engineON;

    //A list of game object postions for docks, this is to keep track of docking positions
    public int dockingPosition;

    //A list that contains all of the space ship size types
    public int spaceShipType;

    //A bool that keeps track of whether or not there's a boat in the shipyard
    public bool shipYardFull = false;

    //A clip that contains a warning sound from the files
    public AudioClip warning;

    public AudioClip explosion;

    //A public game object that refers to where the sound is coming from
    public AudioSource musicSource;

    public AudioSource musicSource2;

    //Calling the use of sprite renderer for warning sprites
    public SpriteRenderer warningSignsRight;
    public SpriteRenderer warningSignsLeft;
    public SpriteRenderer warningSignsShip;

    // Start is called before the first frame update
    void Start()
    {
        //Engine is set to true on start
        engineON = true;

        warningSignsRight.enabled = false;
        warningSignsLeft.enabled = false;
        warningSignsShip.enabled = false;

        //dockingAreaGreen = GetComponent<GameObject>();

        // A test to see if I could get a line drawn using a bool, better use would be the mouse down funciton
        //if(Input.GetMouseButtonUp(0))
        //{
        //    drawingLine = false;
        //}

        //The clip in music source is = to the warning sound effect
        musicSource.clip = warning;
        musicSource2.clip = explosion;
    }

    // Update is called once per frame
    void Update()
    {
        //When the engine is turned on it will move at a certain speed.
        if (engineON == true)
        {
            transform.Translate(new Vector2(0, shipSpeeds) * Time.deltaTime, Space.Self);

        }
        //When the engine is turned off it will cease to move
        if(engineON == false)
        {
            transform.Translate(new Vector2(0, 0) * Time.deltaTime, Space.Self);
        }

        //Here I give the different ship sizes different speeds depending on their list number
        //Medium
        if(spaceShipType == 1)
        {
            shipSpeeds = -0.4f;
        }

        //Large
        if(spaceShipType == 2)
        {
            shipSpeeds = -0.3f;
        }

        //Small
        if (spaceShipType == 0)
        {
            shipSpeeds = -0.5f;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Dock" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(-0.07f, 0.09f);
            engineON = false;      

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock" && shipYardFull == true)
            {
                
                Debug.Log("I'm full, go away");
            }

        }

        if (collision.gameObject.tag == "Dock2" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(1.76f, -1.49f);
            engineON = false;

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock2" && shipYardFull == true)
            {

                Debug.Log("I'm full, go away");
            }

        }

        if (collision.gameObject.tag == "Dock3" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(-6.76f, 1.76f);
            engineON = false;

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock3" && shipYardFull == true)
            {

                Debug.Log("I'm full, go away");
            }

        }

        if (collision.gameObject.tag == "Dock4" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(-5.81f, 2.99f);
            engineON = false;

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock4" && shipYardFull == true)
            {

                Debug.Log("I'm full, go away");
            }

        }

        if (collision.gameObject.tag == "Dock5" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(6.71f, -2.6f);
            engineON = false;

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock5" && shipYardFull == true)
            {

                Debug.Log("I'm full, go away");
            }

        }

        if (collision.gameObject.tag == "Dock6" && shipYardFull == false)
        {
            Debug.Log("Docking");
            transform.position = new Vector2(5.67f, -3.86f);
            engineON = false;

            //The ship yard bool turns to true which prevents other ships from coming in
            shipYardFull = true;

            //Calls the unload crates function
            UnloadCrates();

            //If the trigger detects that the tag is called "Dock" and the shipyard is full, the ship yard can't take anymore boats
            if (collision.gameObject.tag == "Dock6" && shipYardFull == true)
            {

                Debug.Log("I'm full, go away");
            }

        }

        // When the ship leaves the area it will be destroyed
        if (collision.tag == "DeathZone")
         {
            Destroy(gameObject);
         }

        // If the ships come too close together a trigger will happen warning the player of impact
        if(collision.gameObject.tag == "Ship")
        {
            Debug.Log("Too close");

            //When the ships come too close a warning sign will appear on boths sides of the screen
            warningSignsRight.enabled = true;
            warningSignsLeft.enabled = true;

            //When the ships come close together a warning marker will appear above them
            warningSignsShip.enabled = true;

            //When the ships come too close a music source will play an alarm sound
            musicSource.Play();
        }
        
    }

    
    void OnCollisionEnter2D(Collision2D collision)
    {
        //When the ship hits another collider it will turn on a rotation of 90 degrees.
        //transform.Rotate(new Vector3(0, 0, 30) * Time.deltaTime);
        transform.Rotate( 0,  0,  90, Space.Self);

        //If two ships collide with eachother they will destroy eachother
        if (collision.gameObject.tag == "Ship")
        {
            Debug.Log("Crash");

            //The engine is being turned off to prevent them from moving
            engineON = false;

            //Destroying both objects after 5 seconds, "this" applies to both because they are the same game object by tag
            Destroy(this.gameObject, 5);
            musicSource2.Play();

            //When two ships crash all ships will stop in place simulating a game over
            shipSpeeds = 0f;
        }
    }

    void UnloadCrates()
    {
        if (dockingPosition == 1)
        {

        }

        if (dockingPosition == 2)
        {

        }
    }
}
