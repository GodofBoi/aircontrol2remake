﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipSpwan : MonoBehaviour
{
    GameObject randomShip;
    public GameObject[] shipType;
    public float spawnInterval;
    float spawnTime;

    // Start is called before the first frame update
    void Start()
    {
        randomShip = shipType[Random.Range(0, 3)];
        spawnTime = spawnInterval;
    }

    // Update is called once per frame
    void Update()
    {
        spawnTime -= 0.1f;
        SpawnShip();
    }
    void SpawnShip()
    {
        if(spawnTime < 0)
        {
            Instantiate(randomShip, transform.position, transform.rotation);
            randomShip = shipType[Random.Range(0, 3)];
            spawnTime = spawnInterval;
        }
    }
}
