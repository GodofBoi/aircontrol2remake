﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsTwo : MonoBehaviour
{
    public Vector2 objectivePoint;

    public GameObject pointCircle;

    LineRenderer lineRendererComponent;

    //A public variable where the mouse click ends
    public Vector2 mousePoint = new Vector2();

    //Keeps track of the mouse position on the screen
    public Vector2 mousePosition = new Vector2();

    private Camera cam;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        lineRendererComponent = GetComponent<LineRenderer>();

        objectivePoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
            transform.position = Vector2.MoveTowards(transform.position, objectivePoint, speed * Time.deltaTime);

        OnMouseDown();
        OnMouseDrag();
        OnMouseUp();
    }

     void OnMouseDown()
    {
        lineRendererComponent.enabled = true;

        // set the target to the mouse click location
        objectivePoint = mousePoint;

        Instantiate(pointCircle, new Vector3(mousePosition.x, mousePosition.y), Quaternion.identity);
    }

     void OnMouseDrag()
    {
        Vector2 mousePositionInWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

     void OnMouseUp()
    {
        lineRendererComponent.enabled = false;
    }

    //void OnGUI()
    //{
    //    Event currentEvent = Event.current;

    //    mousePosition.x = currentEvent.mousePosition.x;
    //    mousePosition.y = cam.pixelHeight - currentEvent.mousePosition.y;
    //    mousePoint = cam.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 0.0f));


    //}
}
