﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockRotation : MonoBehaviour
{
    //Creates a public float that adjusts the rotations of the rocks z axis
    public float rockRotation;

    // Update is called once per frame
    void Update()
    {
        //Rotates the rock on the screen at every frame
        transform.Rotate(new Vector3(0, 0, rockRotation * Time.deltaTime));
    }

}
