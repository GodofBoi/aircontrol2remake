﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreTracker : MonoBehaviour
{
    //Keeps track of score 
    public static int CrateScore;
    public string CratePrefix;
    public Text CrateScoreText;

    string stringformat = "#";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            CrateScore = CrateScore + 10;
        }

        UpdateHud();
    }

    // Keeps track of the score
    void UpdateHud()
    {
        CrateScoreText.text = CratePrefix + CrateScore.ToString(stringformat);
    }
}
