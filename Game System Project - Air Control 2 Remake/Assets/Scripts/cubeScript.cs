﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cubeScript : MonoBehaviour
{
    // MeshRenderer meshRenderer;
    SpriteRenderer crateRender;
   //public Material material;

    //Creating an int to contain the random range that will dictate the colors
    int crateType;
    
    // Start is called before the first frame update
    void Awake()
    {
        //meshRenderer = GetComponent<MeshRenderer>();

        //Crate type will be at a random range between 0 and 2
        crateType = Random.Range(0, 2);
        crateRender = GetComponent<SpriteRenderer>();

    }


    void Start()
    {

        //Calls the random color function
        setRandomColor();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void setRandomColor()
    {
        //If the crate type is equal to 0 the color will be blue, if the crate type is above 0 it will be green
        if ( crateType == 0)
        {
            // material.color = Color.green;
            crateRender.color = Color.blue;

        }
        else
        {

            // material.color = Color.blue;
            crateRender.color = Color.green;
        }


       
    }
}
